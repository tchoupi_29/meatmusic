import os
import random
import time
import shutil
import pickle
from threading import Thread
import configparser

from moviepy.audio.io.AudioFileClip import AudioFileClip
from mutagen.easyid3 import EasyID3
from mutagen.mp3 import MP3
import mutagen.mp3

from pytube import YouTube
from pytube import helpers
from ytmusicapi import YTMusic

import pygame

import tkinter as tk
import tkinter.messagebox as messagebox
from tkinter.simpledialog import *
from tkinter.ttk import *
from tkinter import filedialog
import TkTreectrl as treectrl


class Music:
    volume = 1.0

    def __init__(self, cheminMusic: str):
        self.cheminMusic = cheminMusic
        self.fichierMp3 = MP3(cheminMusic)

        try:
            self.titre = self.fichierMp3.__getitem__("TIT2")
        except Exception:
            self.titre = None
        try:
            self.album = self.fichierMp3.__getitem__("TALB")
        except Exception:
            self.album = None
        try:
            self.artiste = self.fichierMp3.__getitem__("TPE1")
        except Exception:
            self.artiste = None
        self.duree = Duree(self.fichierMp3.info.length)
        self.duree.calcuer()
        self.paused = False
        self.threadIsRuning = False

    def __eq__(self, other):
        if self.titre == other.titre:
            return True
        else:
            return False

    def __str__(self):
        return f"{self.titre}"

    def threading(self):
        thread = Thread(target=self.play)
        thread.start()

    def play(self):
        self.threadIsRuning = True
        # pygame.mixer.pre_init(44800, -16, 2, 512)
        pygame.mixer.music.load(self.cheminMusic)
        pygame.mixer.music.play()
        self.setDefaultVolume()
        while self.paused or (self.threadIsRuning and pygame.mixer.music.get_busy()):
            pygame.time.Clock().tick(10)
        self.stop()

    def setDefaultVolume(self):
        pygame.mixer.music.set_volume(Music.volume)

    def pause(self):
        pygame.mixer.music.pause()
        self.paused = True

    def unpause(self):
        pygame.mixer.music.unpause()
        self.paused = False

    def stop(self):
        pygame.mixer.music.stop()
        pygame.mixer.music.unload()
        self.threadIsRuning = False

    @classmethod
    def restart(cls):
        pygame.mixer.music.rewind()

    def volUp(self):
        volume = pygame.mixer.music.get_volume()
        pygame.mixer.music.set_volume(volume + 0.1)
        Music.volume = pygame.mixer.music.get_volume()

    def volDown(self):
        volume = pygame.mixer.music.get_volume()
        pygame.mixer.music.set_volume(volume - 0.1)
        Music.volume = pygame.mixer.music.get_volume()

    def setVol(self, vol):
        pygame.mixer.music.set_volume(vol)
        Music.volume = pygame.mixer.music.get_volume()

    def setTime(self, t):
        pygame.mixer.music.set_pos(t)

    @classmethod
    def mute(cls):
        pygame.mixer.music.set_volume(0.0)

    @classmethod
    def unmute(cls):
        pygame.mixer.music.set_volume(Music.volume)

    def busy(self):
        return self.threadIsRuning


class Playlist:
    def __init__(self, nom: str):
        self.listeMusic = []
        self.duree = Duree(0, hour=True)
        self.nom = nom
        self.cheminData = f"./playlist/{self.nom}"
        self.savePlaylist()

    def __eq__(self, other):
        if self.nom == other.nom:
            return True
        else:
            return False

    def getMusicByName(self, nom):
        for music in self.listeMusic:
            if music.titre == nom:
                return music
        return None

    def ajouterMusic(self, music: Music):
        if not self.listeMusic.__contains__(music):
            self.listeMusic.append(music)
            self.duree = self.calcDuree()
            self.savePlaylist()
        else:
            print(f"La musique est déjà dans la playlist : {music}")

    def enleverMusic(self, music: Music):
        if self.listeMusic.__contains__(music):
            self.listeMusic.remove(music)
            self.duree = self.calcDuree()
            self.savePlaylist()
        else:
            print("La musique n'est pas dans la playlist")

    def calcDuree(self):
        dureeTotSec = 0
        for music in self.listeMusic:
            dureeTotSec = dureeTotSec + music.duree.totSec
        duree = Duree(dureeTotSec, hour=True)
        return duree

    def renommer(self, nom: str):
        os.remove(self.cheminData)
        self.nom = nom
        self.cheminData = f"./playlist/{self.nom}"
        self.savePlaylist()

    def savePlaylist(self):
        with open(self.cheminData, 'wb') as playlistFile:
            pickle.dump(self, playlistFile)
        print(f"Sauvegarde de la playlist : {self.nom}")

    def __str__(self):
        listeDesMusique = f"Playlist : {self.nom} :\n"
        for music in self.listeMusic:
            listeDesMusique += f"{music.__str__()}\n"
        return listeDesMusique

    def copyToFolder(self, folder):
        for music in self.listeMusic:
            print(os.path.abspath(music.cheminMusic))
            print(music.titre)
            shutil.copyfile(f"{os.path.abspath(music.cheminMusic)}", f"{folder}/{music.cheminMusic[len('./music/'):]}")


class GestionPlaylist:
    def __init__(self):
        self.listePlayliste = []

    # cette fonction cherche tous les fichiers de playlist et les charge dans des objet playlist puis les ajoutes à sa list play list
    def discoverPlaylist(self):
        playlists = os.listdir("./playlist/")
        for fichier in playlists:
            with open(f"./playlist/{fichier}", 'rb') as playlistFile:
                playlist = pickle.load(playlistFile)
                if not self.listePlayliste.__contains__(playlist):
                    self.listePlayliste.append(playlist)

    # cette fonction se charge de suprimer un objet playlist, elle suprime son fichier de save, sa place dans la liste des playlist et enfin l'objet playlist lui-même
    def suprimerPlaylist(self, playlist: Playlist):
        os.remove(f"./playlist/{playlist.nom}")
        self.listePlayliste.remove(playlist)
        del playlist

    def ajouterPlaylist(self, playlist: Playlist):
        if playlist not in self.listePlayliste:
            self.listePlayliste.append(playlist)

    # retourne la playlist dont le nom est passé en paramètre
    def getPlayListByName(self, nom: str):
        for playlist in self.listePlayliste:
            if playlist.nom == nom:
                return playlist
        return None

    def rename(self, nom: str):
        pass

    def getPlaylistsName(self):
        listeDesPlaylist = []
        for playlist in self.listePlayliste:
            if playlist.nom != "all":
                listeDesPlaylist.append(playlist.nom)
        return listeDesPlaylist

    def supprimerMusic(self, music: Music):
        self.getPlayListByName("all").supprimerMusic(music)
        for playlist in self.listePlayliste:
            if music in playlist.listeMusic:
                playlist.enleverMusic(music)

    def __str__(self):
        listeDesPlaylist = ""
        for playlist in self.listePlayliste:
            listeDesPlaylist += f"{playlist.__str__()}\n"
        return listeDesPlaylist


class PlaylistDl(Playlist):
    def __init__(self, nom: str):
        super().__init__(nom)

    def ajouterMusicDl(self, suprSonAbsent=True):
        musics = os.listdir("./music/")
        for fichier in musics:
            safeTitre = helpers.safe_filename(fichier)[:-3]
            os.rename(f"./music/{fichier}", f"./music/{safeTitre}.mp3")
            # print(safeTitre)
            try:
                music = Music(f"./music/{safeTitre}.mp3")
                dejaLa = False
                for son in self.listeMusic:
                    if music.titre == son.titre:
                        dejaLa = True
                        print(f"Le son est déjà là : {son.titre}")
                if not dejaLa:
                    self.listeMusic.append(music)
            except mutagen.mp3.HeaderNotFoundError:
                try:
                    os.remove(f"./music/{safeTitre}.mp3")
                except:
                    pass
                print(f"fichier corompu : {safeTitre}.mp3")
        if suprSonAbsent:
            for son in self.listeMusic:
                if son.cheminMusic[len("./music/"):] not in musics:
                    self.enleverMusic(son)
                    print(f"Le son est plus la {son.titre}")
        self.savePlaylist()

    def supprimerMusic(self, music: Music):
        os.remove(music.cheminMusic)
        self.enleverMusic(music)
        del music


class Duree:
    def __init__(self, totSec: float, hour=False):
        self.totSec = totSec
        self.secs = 0
        self.mins = 0
        self.hours = 0
        self.hour = hour
        self.calcuer()

    def calcuer(self):
        self.mins, self.secs = divmod(self.totSec, 60)
        self.hours, self.mins = divmod(self.mins, 60)

        self.secs = int(self.secs)
        self.mins = int(self.mins)
        self.hours = int(self.hours)

        if len(str(self.secs)) == 1:
            self.secs = '%02d' % (int('0') + self.secs)
        if self.hour:
            if len(str(self.mins)) == 1:
                self.mins = '%02d' % (int('0') + self.mins)

    def __str__(self):
        if self.hour:
            return f"{self.hours}:{self.mins}:{self.secs}"
        else:
            return f"{self.mins}:{self.secs}"


class Telechargeur:
    def __init__(self, gest: GestionPlaylist):
        self.arret = True
        self.gest = gest
        self.cheminOfMusicDl = []

    def threading(self, url: str, audioDlLabel, nbMusic: str = None, playlist=None):
        # Call downloadAudio function
        t1 = Thread(target=self.downloadAudio, args=(url, audioDlLabel, nbMusic, playlist))
        t1.start()

    def stop(self, playlist=None):
        self.arret = True
        self.gest.getPlayListByName("all").ajouterMusicDl()
        if playlist:
            self.ajouterMusicToPlaylist(playlist)

    def downloadAudio(self, url: str, audioDlLabel, nbMusic: str = None, playlist=None):
        if url:
            self.arret = False
            indispo = []
            nbMusicVrmtDl = 0
            nbMusicDl = 0
            apiMusic = YTMusic()
            playlist_id = None
            video_id = None

            if "youtube.com/playlist?list=" in url:
                playlist_id = url[len("https://www.youtube.com/playlist?list="):]
            elif "music.youtube.com/playlist?list=" in url:
                playlist_id = url[len("https://www.music.youtube.com/playlist?list="):]
            elif "www.youtube.com/watch?v=" in url:
                video_id = url[len("https://www.youtube.com/watch?v="):]
            else:
                playlist_id = url

            if playlist_id is not None:
                try:
                    # Get list of video links
                    playlistItem = apiMusic.get_playlist(playlist_id)

                    # compte les nusiques à dl si il n'y a pas de nbMusic donné
                    if nbMusic == '':
                        maxNbMusicAdl = playlistItem.get("trackCount")
                    else:
                        maxNbMusicAdl = int(nbMusic)

                    playlistItem = apiMusic.get_playlist(playlist_id, maxNbMusicAdl)
                    # Iterate through all video links
                    for i, track in enumerate(playlistItem["tracks"]):
                        if nbMusicDl < maxNbMusicAdl and not self.arret:
                            link = f"https://www.youtube.com/watch?v={track.get('videoId')}"
                            try:
                                yt_obj = YouTube(link)
                                safeTitre = helpers.safe_filename(yt_obj.title)
                                audioDlLabel.config(text=f"Téléchargement de {safeTitre} | {i+1}/{maxNbMusicAdl}", foreground=Windows.fgColorWhite)
                                # download the highest quality audio
                                yt_obj.check_availability()
                                if not self.alreadyDl(f"./music/{safeTitre}.mp3"):
                                    filters = yt_obj.streams.get_audio_only()
                                    filters.download(filename=f"{safeTitre}.mp4", output_path="./music/")
                                    # convertion en mp3
                                    mp4_without_frames = AudioFileClip(f"./music/{safeTitre}.mp4")
                                    mp4_without_frames.write_audiofile(f"./music/{safeTitre}.mp3")
                                    mp4_without_frames.close()
                                    os.remove(f"./music/{safeTitre}.mp4")

                                    # ajouter les metadata
                                    mp3Tags = yt_obj.vid_info
                                    mp3file = MP3(f"./music/{safeTitre}.mp3", ID3=EasyID3)
                                    mp3file['title'] = mp3Tags.get("videoDetails").get("title")
                                    if track.get('album'):
                                        mp3file['album'] = track.get('album').get('name')
                                    if track.get('artists')[0]:
                                        mp3file['artist'] = track.get('artists')[0].get('name')
                                    mp3file.save()

                                    """
                                    mp3file = MP3(f"./music/{yt_obj.title}.mp3", ID3=ID3)
                                    url = mp3Tags.get("videoDetails").get("thumbnail").get("thumbnails")[3].get("url")
                                    image = requests.get(url).content
                                    print(url)
                                    mp3file["APIC"] = APIC(
                                            encoding=3,  # 3 is for utf-8
                                            mime='image/png',  # image/jpeg or image/png
                                            type=PictureType.COVER_FRONT,  # 3 is for the cover image
                                            desc=u'Cover',
                                            data=image
                                        )
                                    mp3file.save()
                                    print(mp3file.pprint())
                                    """

                                    self.cheminOfMusicDl.append(f"./music/{safeTitre}.mp3")

                                    print(f"Téléchargée : {link}")
                                    nbMusicVrmtDl = nbMusicVrmtDl + 1  # incrémentéer nbmusic meme si la musique a pas été dl ou alors créer une vrai variable pour les musique dl pour afficher le nombre vrmt dl
                                else:
                                    print(f"La musique {link} à déjà été téléchargée")
                            except Exception as e:
                                audioDlLabel.config(text=f"Erreur : vidéo indisponnible : {link}", foreground="red")
                                print(f"Erreur : {e} : vidéo indisponnible : {link}")
                                indispo.append(link)
                            nbMusicDl = nbMusicDl + 1

                    messagebox.showinfo("Fin", f"Musiques téléchargées {nbMusicVrmtDl}/{maxNbMusicAdl}")
                    print(f"{len(indispo)} vidéos indisponnilbes veuillez les télécharger mannuelement {indispo}")
                    audioDlLabel.config(text=f"Téléchargement de aucun audio", foreground=Windows.fgColorWhite)
                    # insertion dans le tableau des musiques téléchargées
                    self.stop(playlist)
                except Exception as e:
                    print(f"Erreur : {e}")
                    audioDlLabel.config(text="Erreur : URL eronnée", foreground="red")
                    self.arret = True

            elif video_id is not None:
                link = f"https://www.youtube.com/watch?v={video_id}"
                try:
                    yt_obj = YouTube(link)
                    safeTitre = helpers.safe_filename(yt_obj.title)
                except Exception as e:
                    print(f"Erreur : {e}")
                    audioDlLabel.config(text="Erreur : URL eronnée", foreground="red")
                    self.arret = True
                    yt_obj = None
                    safeTitre = None

                if yt_obj is not None and safeTitre is not None:
                    audioDlLabel.config(text=f"Téléchargement de {safeTitre}", foreground=Windows.fgColorWhite)
                    # on regarde si la vid est dispo et si c pas bon on chope l'exception
                    try:
                        yt_obj.check_availability()
                    except Exception as e:
                        print(f"Erreur : vidéo indisponible : {e}")
                        audioDlLabel.config(text="Erreur : vidéo indisponible", foreground="red")
                        self.arret = True
                    if not self.alreadyDl(f"./music/{safeTitre}.mp3") and not self.arret:
                        try:
                            filters = yt_obj.streams.get_audio_only()
                            filters.download(filename=f"{safeTitre}.mp4", output_path="./music/")

                            # convertion en mp3
                            mp4_without_frames = AudioFileClip(f"./music/{safeTitre}.mp4")
                            mp4_without_frames.write_audiofile(f"./music/{safeTitre}.mp3")
                            mp4_without_frames.close()
                            os.remove(f"./music/{safeTitre}.mp4")

                            # ajouter les metadata
                            songData = apiMusic.get_song(video_id)
                            mp3file = MP3(f"./music/{safeTitre}.mp3", ID3=EasyID3)
                            mp3file['title'] = songData.get("videoDetails").get("title")
                            mp3file.save()

                            self.cheminOfMusicDl.append(f"./music/{safeTitre}.mp3")
                            print(f"Téléchargée : {link}")
                        except Exception as e:
                            audioDlLabel.config(text=f"Erreur : vidéo indisponnible : {link}", foreground="red")
                            print(f"Erreur : {e} : vidéo indisponnible : {link}")
                            indispo.append(link)
                    else:
                        print(f"La musique {link} à déjà été téléchargée")
                messagebox.showinfo("Fin", f"Musique téléchargée !")
                print(f"{len(indispo)} vidéo indisponnilbe veuillez la télécharger mannuelement {indispo}")
                audioDlLabel.config(text=f"Téléchargement de aucun audio", foreground=Windows.fgColorWhite)
                self.stop(playlist)
        else:
            audioDlLabel.config(text="Erreur : pas d'URL donnée", foreground="red")
            self.arret = True

    def alreadyDl(self, chemin):
        directory = './music'
        dejaLa = False
        for filename in os.listdir(directory):
            if chemin == f"{directory}/{filename}":
                dejaLa = True
        return dejaLa

    def ajouterMusicToPlaylist(self, playlist):
        for chemin in self.cheminOfMusicDl:
            playlist.ajouterMusic(Music(chemin))


class Queue:
    def __init__(self):
        self.listeMusic = []
        self.indMusicEnCours = -1
        self.skiped = False
        self.thread = None
        self.time = Duree(0)

    def getMusicByName(self, nom):
        for music in self.listeMusic:
            if music.titre == nom:
                return music
        return None

    def ajouterQueue(self, music: Music):
        self.listeMusic.append(music)

    def lireEnsuite(self, music: Music):
        self.listeMusic.insert(self.indMusicEnCours + 1, music)

    def enleverQueue(self, music: Music):
        if music in self.listeMusic:
            if self.listeMusic.index(music) <= self.indMusicEnCours:
                self.indMusicEnCours = self.indMusicEnCours - 1
            elif self.listeMusic.index(music) > self.indMusicEnCours:
                self.indMusicEnCours = self.indMusicEnCours
            if music.threadIsRuning:
                music.stop()
            self.listeMusic.remove(music)
        if len(self.listeMusic) == 0:
            self.indMusicEnCours = -1
        if self.indMusicEnCours > -1:
            Windows.setLblTitre(f"{self.listeMusic[self.indMusicEnCours].titre} | {self.indMusicEnCours+1}/{len(self.listeMusic)}")

    def viderLaQueue(self):
        if self.indMusicEnCours > -1:
            self.listeMusic[self.indMusicEnCours].stop()
        self.skiped = True
        self.listeMusic = []
        self.indMusicEnCours = -1
        Windows.setLblTitre("")

    def threadingLecture(self):
        self.thread = Thread(target=self.defillementAuto)
        self.thread.start()

    def defillementAuto(self):
        if not len(self.listeMusic) <= 0:
            self.indMusicEnCours = 0
            nbMusicPlayed = 1
            while len(self.listeMusic) > self.indMusicEnCours > -1:
                if 0 <= self.indMusicEnCours < len(self.listeMusic) and len(self.listeMusic) > 0:
                    self.skiped = False
                    self.time.totSec = 0
                    self.listeMusic[self.indMusicEnCours].threading()
                    Windows.setLblTitre(f"{self.listeMusic[self.indMusicEnCours].titre} | {self.indMusicEnCours+1}/{len(self.listeMusic)}")
                    while self.indMusicEnCours >= 0 and self.listeMusic[self.indMusicEnCours].threadIsRuning and not self.skiped:
                        if not self.skiped:
                            Windows.setTimeBar(self)
                        print(f"thread tjrs en cours | sec : {self.time}")
                        time.sleep(1)
                        if self.indMusicEnCours >= 0 and not self.listeMusic[self.indMusicEnCours].paused:
                            self.time.totSec = self.time.totSec + 1
                            self.time.calcuer()
                    if not self.skiped:
                        self.indMusicEnCours = self.indMusicEnCours + 1
                    nbMusicPlayed = nbMusicPlayed + 1
        self.indMusicEnCours = -1
        self.viderLaQueue()
        self.thread = None
        print("c'est la fin du threading")

    def setMusicMtn(self, ind):
        if ind != self.indMusicEnCours and self.indMusicEnCours > -1:
            self.listeMusic[self.indMusicEnCours].stop()
            self.indMusicEnCours = ind
            self.skiped = True

    def musicSuivante(self, event=None):
        if self.indMusicEnCours + 1 < len(self.listeMusic) and len(self.listeMusic) > 0:
            self.listeMusic[self.indMusicEnCours].stop()
            self.indMusicEnCours = self.indMusicEnCours + 1
            self.skiped = True
        else:
            print("C'est déjà la fin de la queue")

    def musicPrecedente(self, event=None):
        if self.indMusicEnCours-1 > 0:
            self.listeMusic[self.indMusicEnCours].stop()
            self.indMusicEnCours = self.indMusicEnCours - 1
            self.skiped = True
        else:
            print("C'est déjà le début de la queue")

    def calcDuree(self):
        dureeTotSec = 0
        for music in self.listeMusic:
            dureeTotSec = dureeTotSec + music.duree.totSec
        duree = Duree(dureeTotSec, hour=True)
        return duree

    def setTime(self, t):
        self.listeMusic[self.indMusicEnCours].setTime(t)

    def pause(self):
        self.listeMusic[self.indMusicEnCours].pause()

    def unpause(self):
        self.listeMusic[self.indMusicEnCours].unpause()

    def setVol(self, vol):
        self.listeMusic[self.indMusicEnCours].setVol(vol)

    def volUp(self):
        self.listeMusic[self.indMusicEnCours].volUp()

    def volDown(self):
        self.listeMusic[self.indMusicEnCours].volDown()

    def mute(self):
        self.listeMusic[self.indMusicEnCours].mute()

    def unmute(self):
        self.listeMusic[self.indMusicEnCours].unmute()

    def __str__(self):
        listeDesMusique = "Queue :\n"
        for music in self.listeMusic:
            listeDesMusique += f"{music.__str__()}\n"
        return listeDesMusique


class Windows(tk.Tk):
    config = configparser.ConfigParser()
    config.read_file(open(r'config.txt'))
    bgColorGrey = config.get('Couleurs de fonds', 'Fond')
    bgColorGreyLight = config.get('Couleurs de fonds', 'FondClair')
    bgColorGreen = config.get('Couleurs de fonds', 'Bien')
    bgColorGreenLight = config.get('Couleurs de fonds', 'BienClair')
    bgColorRed = config.get('Couleurs de fonds', 'Mal')
    bgColorRedLight = config.get('Couleurs de fonds', 'MalClair')
    bgColorYellow = config.get('Couleurs de fonds', 'Principale')
    bgColorYellowLight = config.get('Couleurs de fonds', 'PrincipaleClair')

    fgColorWhite = config.get('Couleurs de texte', 'Clair')
    fgColorBlack = config.get('Couleurs de texte', 'Fonce')

    police = config.get('Polices', 'Texte')
    policeTitre = config.get('Polices', 'Titre')

    """
    bgColorGrey = "#0B2140"
    bgColorGreyLight = "#18488C"
    bgColorGreen = "#35B554"
    bgColorGreenLight = "#3ED664"
    bgColorRed = "#E05334"
    bgColorRedLight = "#ED6349"
    bgColorYellow = "#DEA11E"
    bgColorYellowLight = "#EDC221"

    fgColorWhite = "#F0FFFB"
    fgColorBlack = "#000000"

    police = "Ubuntu 11"
    policeTitre = "Ubuntu 15 bold"
    """

    lblTitre = None
    timeBar = None
    lblTime = None

    def __init__(self, gest: GestionPlaylist, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        self.gest = gest
        self.queue = Queue()
        self.frameUp = None

        self.playBtnImg = tk.PhotoImage(file="./images/playBtnImg.png")
        self.pauseBtnImg = tk.PhotoImage(file="./images/pauseBtnImg.png")
        self.suivBtnImg = tk.PhotoImage(file="./images/suivBtnImg.png")
        self.precBtnImg = tk.PhotoImage(file="./images/precBtnImg.png")
        self.stopBtnImg = tk.PhotoImage(file="./images/stopBtnImg.png")

        # Adding a title to the window
        self.wm_title("MeatMusic")
        self.iconbitmap(default='./playButton.ico')
        self.minsize(500, 500)

        # creating a frame and assigning it to container
        self.container = tk.Frame(self, height=400, width=1600)
        # specifying the region where the frame is packed in root
        self.container.pack(side="top", fill="both", expand=True)

        # configuring the location of the container using grid
        self.container.grid_rowconfigure(0, weight=8)
        self.container.grid_rowconfigure(1, weight=1, minsize=50)
        self.container.grid_columnconfigure(0, weight=1)
        self.container.grid_columnconfigure(1, weight=6)

        # crete leftpannel
        leftPannel = tk.Frame(self.container, bg=Windows.bgColorGrey)
        leftPannel.grid(row=0, column=0, sticky="nsew", rowspan=2)

        leftPannel.grid_rowconfigure(0, weight=1)
        leftPannel.grid_rowconfigure(1, weight=1)
        leftPannel.grid_columnconfigure(0, weight=1, minsize=100)

        leftPannelTop = tk.Frame(leftPannel, bg=Windows.bgColorGrey)
        leftPannelTop.grid(row=0, column=0, sticky="new")
        leftPannelTop.grid_rowconfigure(0, weight=1)
        leftPannelTop.grid_rowconfigure(1, weight=1)
        leftPannelTop.grid_rowconfigure(2, weight=1)
        leftPannelTop.grid_rowconfigure(3, weight=1)
        leftPannelTop.grid_columnconfigure(0, weight=1)

        # create downPannel
        downPannel = tk.Frame(self.container, bg=Windows.bgColorGrey)
        downPannel.grid(row=1, column=1, sticky="nsew")

        downPannel.grid_rowconfigure(0, weight=1)

        downPannel.grid_columnconfigure(0, weight=1)
        downPannel.grid_columnconfigure(1, weight=3)
        downPannel.grid_columnconfigure(2, weight=1)

        downPannelLeft = tk.Frame(downPannel, bg=Windows.bgColorGrey)
        downPannelLeft.grid(row=0, column=0, sticky="nsew")

        downPannelLeft.grid_rowconfigure(0, weight=1)
        downPannelLeft.grid_columnconfigure(0, weight=1)

        Windows.setLblTitre("", cont=downPannelLeft)

        downPannelMidle = tk.Frame(downPannel, bg=Windows.bgColorGrey)
        downPannelMidle.grid(row=0, column=1, sticky="nsew")

        downPannelMidle.grid_columnconfigure(0, weight=1)
        downPannelMidle.grid_columnconfigure(1, weight=1)
        downPannelMidle.grid_columnconfigure(2, weight=1)
        downPannelMidle.grid_columnconfigure(3, weight=1)
        downPannelMidle.grid_columnconfigure(4, weight=1)
        downPannelMidle.grid_rowconfigure(0, weight=1)
        downPannelMidle.grid_rowconfigure(1, weight=1)

        self.btnLast = tk.Button(downPannelMidle, image=self.precBtnImg, borderwidth=0, command=self.queue.musicPrecedente, bg=Windows.bgColorGrey, activebackground=Windows.bgColorGrey)
        self.btnLast.image = self.precBtnImg
        self.btnLast.grid(row=0, column=0, sticky="w")
        self.bind('<Left>', self.queue.musicPrecedente)

        self.btnStop = tk.Button(downPannelMidle, image=self.stopBtnImg, command=self.viderLaQueue, borderwidth=0, bg=Windows.bgColorGrey, activebackground=Windows.bgColorGrey)
        self.btnStop.image = self.stopBtnImg
        self.btnStop.grid(row=0, column=1, sticky="e")

        self.btnPlay = tk.Button(downPannelMidle, image=self.playBtnImg, borderwidth=0, font=Windows.police, command=self.play, bg=Windows.bgColorGrey, activebackground=Windows.bgColorGrey)
        self.bind('<space>', self.play)
        self.btnPlay.image = self.playBtnImg
        self.btnPlay.grid(row=0, column=2, sticky="e")

        self.btnNext = tk.Button(downPannelMidle, image=self.suivBtnImg, borderwidth=0, command=self.queue.musicSuivante, bg=Windows.bgColorGrey, activebackground=Windows.bgColorGrey)
        self.btnNext.image = self.suivBtnImg
        self.btnNext.grid(row=0, column=5)
        self.bind('<Right>', self.queue.musicSuivante)

        """
        fond = b'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x02\x00\x00\x00\x02\x08\x06\x00\x00\x00r\xb6\r$\x00\x00\x00\tpHYs\x00\x00\x0e\xc3\x00\x00\x0e\xc3\x01\xc7o\xa8d\x00\x00\x00\x19tEXtSoftware\x00www.inkscape.org\x9b\xee<\x1a\x00\x00\x00\x15IDAT\x08\x99c\\\xb5j\xd5\x7f\x06\x06\x06\x06&\x06(\x00\x00.\x08\x03\x01\xa5\\\x04^\x00\x00\x00\x00IEND\xaeB`\x82'
        curseur = b'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x000\x00\x00\x000\x08\x06\x00\x00\x00W\x02\xf9\x87\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\x00\x00\tpHYs\x00\x00\x1d\x87\x00\x00\x1d\x87\x01\x8f\xe5\xf1e\x00\x00\x00\x19tEXtSoftware\x00www.inkscape.org\x9b\xee<\x1a\x00\x00\x05\xa4IDATh\x81\xd5\x9a\xcfo\x1bE\x14\xc7?3\xcez\x13\xb7k7\xb1\x9c4\x07\xdaT \xe8\x11*\x0e\xe5\x04A\xaa\x80\x1cP\x13\xe2\xcd\x85\x1eB\xab\x1e\x81\x0b\xe2ohOpD\xa8p(\x95\xaa\x8d\xa0\xad\x04F\xfc\x90\xc2\xad\x1c\xa0\xaa8\x15\xd1\xaa\xa4\x97\xd6?b\xe2\x9f\xb1\xb3\xf6>\x0e\xf6\x86%q\x9a8mj\xf7s\xb2gv\xc7\xdf7\x9e\x9d7\xef\xbdU<\x06\x16\x17\x17\x07\xd2\xe9\xf4\xcb\xa1P\xe8\xb8\xe7yG\x95R\xcf\x01\x07\x80X\xfb\x92\x02\xb0""\xb7\xb5\xd6\xb7\x94R\xd7\xe3\xf1\xf8\xef\x93\x93\x93\x8dG\xfdm\xf5(\xa2s\xb9\xdc\x9b\xc0)\x11y\x0b\xb0\xba\x1c\xa2(")\xa5\xd4W\x89D\xe2\x87\xdd\x1a\xd3\xb5\x01\x8e\xe3\x0c\x01\xa7\x81\x8f\x80C~\xbba\x18\x0c\r\ra\x9a&\x86a\x10\n\x85\xd0Z\x03\xe0y\x1e\xcdf\x13\xd7u\xa9\xd7\xeb\xd4j5\xd6\xd6\xd6\x82\xc3\xde\x13\x91\xf3\xd5j\xf5\xc2\xfc\xfc|m\xcf\x0cXXX\x98\x11\x91O\x80g|\xd1\xd1h\x14\xcb\xb2\x18\x18\x18\xe8f(\x1a\x8d\x06\xa5R\x89b\xb1\x88\xeb\xba~\xf3\x92R\xea\x83d2ym\xa7\xe3\xec\xc8\x80K\x97.\r\x1b\x86q\x01\x98\x060M\x93\x91\x91\x11\xf6\xed\xdb\xd7\x95\xe8N\x88\x08\xd5j\x95|>O\xbd^\xf7\x9b\xbf6\x0c\xe3\xcc\xf4\xf4\xf4\xcav\xf7ok\xc0\xe5\xcb\x97\x8fi\xad\xbf\x01\x0ek\xad\x89\xc7\xe3D\xa3Q\x94\xda\xf5\xe3\xb3%\x85B\x81\xe5\xe5e<\xcf\x03\xb8\x0b\xcc\xd8\xb6}\xf3a\xf7<T\xc5\xc2\xc2\xc2k"r\r\x88\x9a\xa6\xc9\xf8\xf8x\xd7K\xa5[\x1a\x8d\x06\x0f\x1e<\xa0V\xab\x01\x94\x95R\xef$\x93\xc9\x1f\xb7\xba~K\x03\x1c\xc7y\x1dH\x01\xa6eY\x8c\x8e\x8e\xee\xc9\xacwBD\xc8d2\x94J%\x80\x9a\x88L\xcd\xcd\xcd-v\xba\xb6\xa3\xa2\xf6\xb2\xf9\x05\xb0b\xb1\x18\x89Db\xef\xd4n\x81\x88\x90\xcb\xe5(\x14\n\x00E\xe0\xd5N\xcbi\x93\x01W\xae\\9\xe0\xba\xee\r\xe0\xc8\x93\x9e\xf9Nd2\x19\x8a\xc5"\xc0=\xe0%\xdb\xb6\xf3\xc1~\xbd\xf1\x06\xd7u\xbf\x00\x8e\x98\xa6\xd9s\xf1\x00\x89D\x82\xc1\xc1Ah\xf9\x9c\xcf6\xf6\xff\xcf\x00\xc7q\xa6\x81i\xad5\xe3\xe3\xe3=\x17\x0f\xa0\x94bll\xccw\x8a\xb3\x8e\xe3\xbc\x1d\xec_7\xa0\xeda?\x01\x88\xc7\xe3{\xbe\xdbt\x83a\x18\xc4\xe3q\xff\xeb\xa7\xa9T\xca\xf4\xbf\x04\xff\x81\xd3\xc0!\xd34\x89F\xa3OR\xdf\x8e\x88\xc5b\x98\xa6\t0Q.\x97\xdf\xf3\xdb5\xb4\x0ef\xb4\xce6\x0c\x0f\x0f\xf7\xc5\xd2\xe9\xc4\xc8\xc8\x88\xff\xf1\xe3\xb6\xe6\x96\x01\xd9l\xf6\r\xe0\x90a\x18\xec\xdf\xbf\xbfG\xf2\xb6\'\x12\x89\x10\x0e\x87\x01\x0e\xe7r\xb9\x13\xf0\xdf\x12:\x05\xf4\xe5\xd2\t\xa2\x94\nN\xf0)\x00\xb5\xb8\xb88\x90\xcdf\x97\x81\xe8\xc4\xc4D_=\xbc\x9dp]\x97\xa5\xa5%h\x05Iq\x9dN\xa7_\x06\xa2\x86a\xf4\xbdxh\xedH\x86a\x00\xc4<\xcf;\xa6C\xa1\xd0q\x80\xa1\xa1\xa1\xde*\xeb\x82H$\x02\x80R\xea\x15\xedy\xdeQ\xc0\xdf\xa2\x9e\n\xda\xff\x00\xc0\x0b\xba\x1d\x80\x07\x1b\xfb\x9e\xf6N\x84R\xeay\r\x0c\x03\x84B\xa1^j\xea\x8a\x80\xd6\x03\x1a\xd8\x0f\xf4\xad\xf3\xeaD@\xab\xb5\xe94\xfa\xb4\xa1\x812\xb4\x02\x88\xa7\x85\x80\xd6\x92\x06\xfe\x01h6\x9b=\x13\xd4-\x01\xad\xffh\x11\xb9\r\x04s3}\x8f\xafUD\xfe\xd2Z\xeb[@0\'\xd3\xf7\x04\xb4\xfe\xa9\x9b\xcd\xe6\xaf\x00\xab\xab\xab\xbdS\xd4%\xbeV\x11\xb9\xae\xc7\xc6\xc6~\x03\n\xae\xeb\xd2h<r\xb2x\xcfi4\x1a\xfe\x12Z\xd1Z\xdf\xd0\x93\x93\x93\r\x11\xf9\x1e\xf0\xf30}M@c\xca\xb6\xed\xa6\xef\x07.\x02~\xfa\xa2o\x11\x91\xa0\x01\x17\xa1\x1d\xd0\x8c\x8e\x8e\xfe\x08\xdcs]\x97J\xa5\xd2#y\xdbS\xa9T\xfc\xb4\xfc\xdf\x89D\xe2gh\x1b\xd0^F\xe7\x01\xf2\xf9\xfc\xd6#\xf4\x18_\x9b\x88\x9c\xf3\x0b"\xebG\x89j\xb5z\x01X\xaa\xd7\xeb~:\xaf\xafXYY\xf1g\xff\xaeeY_\xfa\xed\xeb\x06\xcc\xcf\xcf\xd7D\xe4C\x80\xe5\xe5\xe5\xberl\x8dFc}\xf6\x95R\xefOMM\xad;\x82\xff\x1d\xe6\xe6\xe6\xe6\xae\x02_{\x9eG:\x9d\xee\x8b\xf3\x91\x88p\xff\xfe}<\xcfC)\xe5$\x93\xc9o\x83\xfd\x9bN\xa3\x86a\x9c\x01\xee\xd6j\xb5\xbe0"\x93\xc9\xf8\x9e\xf7\x8e\x88\x9c\xdd\xd8\xbf\xc9\x80vYg\x06(\x96\xcber\xb9\\O\x8c\x10\x11\xb2\xd9\xac\xbfm\x16\xb4\xd63\xb6moz8;\xc6\x03\xb6m\xdf\x14\x91\x93@\xadP(\x90\xc9d\x9e\xa8\x11"B:\x9d\xf67\x93\x9aR\xea\xe4\xec\xec\xec\x1f\x9d\xae\xddI\x89\xe9*\x103M\x93\x83\x07\x0f\xeey\xec\xbc\xb1\xc4D\xabN\xf6\xd3V\xd7o\x1bG:\x8e\xf3"\xf0\rp\xc4/\xf2\xc5b\xb1\xedn\xdb\x15\x1b\x8a|w\xb4\xd63[\xcd\xbc\xcf\x8e\x02\xe1v\xd5\xe6s`\x16\xfe+\xb3F"\x91G\x8e\xa5E\x84J\xa5B>\x9f_/~+\xa5\x1c\x119\xdbi\xcdo\xa4\xab_o\x17\x17>\x05&\xa0\x95\xde\xb0,kW\x85n\xd7u)\x97\xcb\x94J\xa5`\xd5\xfe\xaeR\xea\xfd\x8d[\xe5\xc3\xe8z\xfaR\xa9\x94\xd9\xce\xcf\x7f\x0c\x1c\xf6\xdb\xc3\xe10\x83\x83\x83\x84\xc3a\xc2\xe1\xf0\xfa\xab\x06"\x82\x88\xac\x1f\x83\xd7\xd6\xd6X]]\xdd\xe8(\xff\x16\x91s\x96e}\x19tR{b\x80O\xfbe\x8f\x13"\xf2.0E\xeb\xed\x94nX\x11\x91\xef\x94R_\x01?\xd9\xb6\xbd\xab\xa0\xfc\xb1$\x83\x1c\xc7\ty\x9ewL)\xf5\x8aR\xea(\xf0,\x10\x07\xfc|}\x11X\xa6\xe5\x8cn\x89\xc8u\xad\xf5\x8d\xdd\x8a\x0e\xf2/#\xf8\x81 \xf2;_\x08\x00\x00\x00\x00IEND\xaeB`\x82'

        fondImg = tk.PhotoImage(data=fond)
        curseurImg = tk.PhotoImage(data=curseur)
        style = Style(self)
        # create scale elements
        style.element_create('custom.Scale.trough', 'image', fondImg)
        style.element_create('custom.Scale.slider', 'image', curseurImg)
        # create custom layout
        style.layout('custom.Horizontal.TScale',
                     [('custom.Scale.trough', {'sticky': 'ew'}),
                      ('custom.Scale.slider',
                       {'side': 'left', 'sticky': ''
                        })])
        """

        timeBar = TimeBarScale(downPannelMidle, from_=0, to=1, orient=tk.HORIZONTAL, command=self.setTime)
        timeBar.grid(row=1, column=0, columnspan=6, sticky="ew", padx=30)
        Windows.timeBar = timeBar

        lblTime = tk.Label(downPannelMidle, text="0:00 / 0:00", font=Windows.police, bg=Windows.bgColorGrey, fg=Windows.fgColorWhite)
        lblTime.grid(row=0, column=3, sticky="")
        Windows.lblTime = lblTime

        downPannelRight = tk.Frame(downPannel, bg=self.bgColorGrey)
        downPannelRight.grid(row=0, column=2, sticky="nsew")

        downPannelRight.grid_columnconfigure(0, weight=1)
        downPannelRight.grid_rowconfigure(0, weight=3)
        downPannelRight.grid_rowconfigure(1, weight=1)

        # tk.Button(downPannelRight, text="Muet", font=Windows.police, command=Music.mute, bg=Windows.bgColorYellow, activebackground=Windows.bgColorYellowLight).grid(row=0, column=0, sticky="")
        # tk.Button(downPannelRight, text="Bruiant", font=Windows.police, command=Music.unmute, bg=Windows.bgColorYellow, activebackground=Windows.bgColorYellowLight).grid(row=0, column=1, sticky="")
        self.volBar = Scale(downPannelRight, from_=0, to=1, orient=tk.HORIZONTAL, command=self.setVol, value=Music.volume)
        self.volBar.grid(row=0, column=0, columnspan=2, sticky="ew", padx=30, rowspan=2)

        # add buton to grid on left pannel at 1,1
        btnTelecharger = tk.Button(leftPannelTop, text="Télécharger", font=Windows.police, command=lambda: self.show_frame(FenTelecharger), fg=Windows.fgColorBlack, bg=Windows.bgColorYellow, activebackground=Windows.bgColorYellowLight)
        btnTelecharger.grid(row=0, column=0, sticky="ew")

        btnListeDeLecture = tk.Button(leftPannelTop, text="Liste de lecture", font=Windows.police, command=lambda: self.show_frame(FenQueue), fg=Windows.fgColorBlack, bg=Windows.bgColorYellow, activebackground=Windows.bgColorYellowLight)
        btnListeDeLecture.grid(row=1, column=0, sticky="ew")

        btnTout = tk.Button(leftPannelTop, text="Toute la musique", font=Windows.police, command=lambda: self.show_frame(FenTout), fg=Windows.fgColorBlack, bg=Windows.bgColorYellow, activebackground=Windows.bgColorYellowLight)
        btnTout.grid(row=2, column=0, sticky="ew")

        btnRechercher = tk.Button(leftPannelTop, text="Rechercher", font=Windows.police, command=lambda: self.show_frame(FenRechercher), fg=Windows.fgColorBlack, bg=Windows.bgColorYellow, activebackground=Windows.bgColorYellowLight)
        btnRechercher.grid(row=3, column=0, sticky="ew")

        # creation d'un frame pour les playlist
        framePlaylist = tk.Frame(leftPannel)
        framePlaylist.grid(row=1, column=0, sticky="nsew")

        framePlaylist.grid_rowconfigure(0, weight=1)
        framePlaylist.grid_rowconfigure(1, weight=7)
        framePlaylist.grid_rowconfigure(2, weight=2)
        framePlaylist.grid_columnconfigure(0, weight=1)

        titrePlaylist = tk.Label(framePlaylist, text="Playlist :", font=Windows.policeTitre, bg=Windows.bgColorGrey, fg=Windows.fgColorWhite)
        titrePlaylist.grid(row=0, column=0, sticky="nsew", columnspan=2)

        self.afficherPlaylists(framePlaylist)

        # btn gestion playlist
        btnCreer = tk.Button(framePlaylist, text="Créer une\nnouvelle Playlist", font=Windows.police, command=lambda: self.ajouterPlaylist(framePlaylist), fg=Windows.fgColorBlack, bg=Windows.bgColorGreen, activebackground=Windows.bgColorGreenLight)
        btnCreer.grid(row=2, column=0, columnspan=2, sticky="nsew")

        # We will now create a dictionary of frames
        self.frames = {FenTelecharger: FenTelecharger(self.container, self.gest)}

        self.frames[FenTelecharger].grid(row=0, column=1, sticky="nsew")

        self.frames[FenTout] = FenTout(self.container, self.gest.getPlayListByName("all"), self.queue, self.gest)
        self.frames[FenTout].grid(row=0, column=1, sticky="nsew")

        self.frames[FenPlaylist] = FenPlaylist(self.container, self.gest.getPlayListByName("all"), self.queue, self.gest)

        self.frames[FenRechercher] = FenRechercher(self.container, self.gest)
        self.frames[FenRechercher].grid(row=0, column=1, sticky="nsew")

        self.frames[FenQueue] = FenQueue(self.container, self.queue)
        self.frames[FenQueue].grid(row=0, column=1, sticky="nsew")

        # Using a method to switch frames
        self.show_frame(FenTout)

    def show_frame(self, cont, playlist=None):
        frame = self.frames[cont]
        # raises the current frame to the top
        if cont == FenQueue:
            frame.afficherQueue()
        elif cont == FenTout:
            frame.afficherPlaylist()
        elif cont == FenPlaylist:
            self.frames[FenPlaylist] = FenPlaylist(self.container, playlist, self.queue, self.gest)
            self.frames[FenPlaylist].grid(row=0, column=1, sticky="nsew")
            frame = self.frames[FenPlaylist]
            frame.afficherPlaylist()
        self.frameUp = cont
        frame.tkraise()

    def afficherPlaylists(self, framePlaylist):
        # creation de la liste de playlist sous forme de boutons
        canvas = tk.Canvas(framePlaylist, bg=Windows.bgColorGrey, highlightthickness=0)
        canvas.delete('all')
        vsb = tk.Scrollbar(framePlaylist, orient="vertical", command=canvas.yview)
        framePlaylist.grid_rowconfigure(0, weight=1)
        framePlaylist.grid_columnconfigure(0, weight=1)
        canvas.configure(yscrollcommand=vsb.set, width=120, height=60)
        canvas.grid(row=1, column=0, sticky="nsew")
        vsb.grid(row=1, column=1, sticky="ns")
        pxDecal = 0
        for playlist in self.gest.listePlayliste:
            if not playlist.nom == "all":
                btnPlay = tk.Button(canvas, text=f"{playlist.nom}", font=Windows.police, width=11, command=lambda play=playlist: self.show_frame(FenPlaylist, play), fg=Windows.fgColorBlack, bg=Windows.bgColorYellow, activebackground=Windows.bgColorYellowLight)
                canvas.create_window(0, pxDecal, anchor="nw", window=btnPlay)

                btnMod = tk.Button(canvas, text="Rename", fg=Windows.fgColorWhite, bg=Windows.bgColorGreyLight, activebackground=Windows.bgColorGrey, font=Windows.police, command=lambda play=playlist: self.renommerPlaylist(framePlaylist, play))
                canvas.create_window(btnPlay.winfo_reqwidth()+5, pxDecal, anchor="nw", window=btnMod)

                btnSuppr = tk.Button(canvas, text="-", fg=Windows.fgColorBlack, bg=Windows.bgColorRed, activebackground=Windows.bgColorRedLight, font=Windows.police, command=lambda play=playlist: self.supprimerPlaylist(framePlaylist, play))
                canvas.create_window(btnMod.winfo_reqwidth()+5 + btnPlay.winfo_reqwidth()+5, pxDecal, anchor="nw", window=btnSuppr)

                pxDecal = pxDecal + 32
        canvas.bind("<Configure>", lambda event: canvas.configure(scrollregion=canvas.bbox("all")))

    def renommerPlaylist(self, framePlaylist, playlist):
        askString = CustomAksString(self, "Rennomer la playlist", f"Donnez un nouveau nom à la playlist : '{playlist.nom}'\n(10 caractères max)")
        nom = askString.result
        if nom:
            playlist.renommer(nom)
            self.afficherPlaylists(framePlaylist)
            if self.frameUp == FenPlaylist and self.frames[FenPlaylist].playlist.nom == nom:
                self.show_frame(FenPlaylist, playlist)

    def ajouterPlaylist(self, framePlaylist):
        askString = CustomAksString(self, "Créer une Playlist", "Donnez un nom\n(max 10 caractères)")
        nom = askString.result
        if nom:
            self.gest.ajouterPlaylist(Playlist(nom))
            self.afficherPlaylists(framePlaylist)

    def supprimerPlaylist(self, framePlaylist, playlist):
        rep = tk.messagebox.askokcancel("Suppression d'une playlist", f"La playlist '{playlist.nom}' va être supprimée.\nAttention cette opération est définitive !")
        if rep:
            self.gest.suprimerPlaylist(playlist)
            self.afficherPlaylists(framePlaylist)
            if self.frameUp == FenPlaylist and self.frames[FenPlaylist].playlist.nom == playlist.nom:
                self.show_frame(FenTout)

    def setVol(self, _=None):
        if self.queue.indMusicEnCours > -1:
            self.queue.setVol(self.volBar.get())

    def play(self, event=None):
        if self.queue.indMusicEnCours == -1:
            self.btnPlay.config(image=self.playBtnImg)
            self.btnPlay.image = self.playBtnImg
            if len(self.queue.listeMusic) > 0:
                self.queue.threadingLecture()
                self.btnPlay.config(image=self.pauseBtnImg)
                self.btnPlay.image = self.pauseBtnImg
        elif self.queue.indMusicEnCours > -1 and not self.queue.listeMusic[self.queue.indMusicEnCours].paused:
            self.queue.pause()
            self.btnPlay.config(image=self.playBtnImg)
            self.btnPlay.image = self.playBtnImg
        elif self.queue.indMusicEnCours > -1 and self.queue.listeMusic[self.queue.indMusicEnCours].paused:
            self.queue.unpause()
            self.btnPlay.config(image=self.pauseBtnImg)
            self.btnPlay.image = self.pauseBtnImg

    def viderLaQueue(self):
        self.queue.viderLaQueue()
        self.btnPlay.config(image=self.playBtnImg)
        self.btnPlay.image = self.playBtnImg
        Windows.timeBar.config(to=1, value=0)
        Windows.lblTime["text"] = f"0:00 / 0:00"
        if self.frameUp == FenQueue:
            self.show_frame(FenQueue)

    @classmethod
    def setLblTitre(cls, titre, cont=None):
        if cont:
            cls.lblTitre = tk.Label(cont, text=f"pas de music", wraplength=300, font=Windows.police, bg=Windows.bgColorGrey, fg=Windows.fgColorWhite)
            cls.lblTitre.grid(row=0, column=0, sticky="ns")
        cls.lblTitre["text"] = titre

    @classmethod
    def setTimeBar(cls, queue):
        Windows.timeBar.config(to=queue.listeMusic[queue.indMusicEnCours].duree.totSec, value=queue.time.totSec)
        Windows.lblTime["text"] = f"{queue.time} / {queue.listeMusic[queue.indMusicEnCours].duree}"

    def setTime(self, _=None):
        if self.queue.indMusicEnCours > -1:
            timetoSet = Windows.timeBar.get()
            pygame.mixer.music.set_pos(timetoSet)
            self.queue.time.totSec = int(timetoSet)
            self.queue.time.calcuer()
            Windows.lblTime["text"] = f"{self.queue.time} / {self.queue.listeMusic[self.queue.indMusicEnCours].duree}"


class FenTelecharger(tk.Frame):
    def __init__(self, parent, gest):
        tk.Frame.__init__(self, parent, bg=Windows.bgColorGreyLight)
        self.gest = gest
        self.dl = Telechargeur(self.gest)

        self.grid_columnconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=2)

        self.grid_rowconfigure(0, weight=1)
        self.grid_rowconfigure(1, weight=1)
        self.grid_rowconfigure(2, weight=1)
        self.grid_rowconfigure(3, weight=1)
        self.grid_rowconfigure(4, weight=1)

        # Add Label
        tk.Label(self, text="Téléchargement", font=Windows.policeTitre, bg=Windows.bgColorGreyLight, fg=Windows.fgColorWhite).grid(row=0, column=0, columnspan=2, sticky="w", padx=20)
        tk.Label(self, text="Donnez l'URL d'une Playlist", font=Windows.police, bg=Windows.bgColorGreyLight, fg=Windows.fgColorWhite).grid(row=1, column=0, sticky="e", ipadx=40)

        # Add Entry box
        playlistId = tk.Entry(self, width=80, font=Windows.police)
        playlistId.grid(row=1, column=0, sticky="w", padx=20)
        tk.Label(self, text="Nombre de musiques à téléchagrer (laissez vide pour tout télécharger)", font=Windows.police, bg=Windows.bgColorGreyLight, fg=Windows.fgColorWhite).grid(row=2, column=0, sticky="w", ipadx=70)

        nbMusicAdl = tk.Entry(self, width=5, font=Windows.police)
        nbMusicAdl.grid(row=2, column=0, sticky="w", padx=20)

        self.nomPlaylist = None
        self.askPlaylist = None

        tk.Button(self, text="Ajouter les musiques à une playlist", command=self.selectPlaylist, font=Windows.police, fg=Windows.fgColorBlack, bg=Windows.bgColorYellow, activebackground=Windows.bgColorYellowLight).grid(row=3, column=0, sticky="w", padx=20)
        self.lblPlaylist = tk.Label(self, text="Aucune playlist séléctionnée", font=Windows.police, bg=Windows.bgColorGrey, fg=Windows.fgColorWhite)
        self.lblPlaylist.grid(row=3, column=0, sticky="w", padx=270)

        self.audioDlLabel = tk.Label(self, text=f"Téléchargement de aucun audio", wraplength=300, bg=Windows.bgColorGrey, fg=Windows.fgColorWhite, font=Windows.police)
        self.audioDlLabel.grid(row=4, column=0, sticky="")

        download_start = tk.Button(self, text="Télécharger",  width=20, font=Windows.police, command=lambda: self.creerDl(playlistId.get(), nbMusicAdl.get()), fg=Windows.fgColorBlack, bg=Windows.bgColorGreen, activebackground=Windows.bgColorGreenLight)
        download_start.grid(row=5, column=0, sticky="w", padx=20, pady=20)
        download_stop = tk.Button(self, text="Arrêter", width=20, font=Windows.police, command=lambda: self.stopDl(), fg=Windows.fgColorBlack, bg=Windows.bgColorRed, activebackground=Windows.bgColorRedLight)
        download_stop.grid(row=5, column=0, sticky="e", pady=20)

    def creerDl(self, url, nbMusic):
        if self.dl.arret is True:
            if self.nomPlaylist:
                playlist = self.gest.getPlayListByName(self.nomPlaylist)
                self.dl.threading(url, self.audioDlLabel, nbMusic=nbMusic, playlist=playlist)
            else:
                self.dl.threading(url, self.audioDlLabel, nbMusic=nbMusic, playlist=None)

    def stopDl(self):
        self.dl.stop()

    def selectPlaylist(self):
        self.askPlaylist = ComboDialog(self, self.gest, okTxt="Ajouter", cancelTxt="Enlever")
        self.nomPlaylist = self.askPlaylist.result
        if self.nomPlaylist:
            self.lblPlaylist["text"] = f"Playlist sélectionnée : {self.nomPlaylist}"
        else:
            self.lblPlaylist["text"] = f"Aucune playlist séléctionnée"


class FenTout(tk.Frame):
    def __init__(self, parent, playlist, queue, gest):
        tk.Frame.__init__(self, parent, bg=Windows.bgColorGreyLight)
        self.playlist = playlist
        self.queue = queue
        self.gest = gest

        self.grid_columnconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=1)
        self.grid_columnconfigure(2, weight=1)

        self.grid_rowconfigure(0, weight=1)
        self.grid_rowconfigure(1, weight=3)

        tk.Label(self, text=f"Toute la musique", font=Windows.policeTitre, bg=Windows.bgColorGreyLight, fg=Windows.fgColorWhite).grid(row=0, column=0, sticky="nsw", padx=20)
        tk.Button(self, text="Tout ajouter\nà la liste de lecture", command=self.lire, font=Windows.police, fg=Windows.fgColorBlack, bg=Windows.bgColorYellow, activebackground=Windows.bgColorYellowLight).grid(row=0, column=0, padx=10, sticky="e")
        tk.Button(self, text="Tout ajouter\nen aléatoire à la liste de lecture", command=self.lireAlea, font=Windows.police, fg=Windows.fgColorBlack, bg=Windows.bgColorYellow, activebackground=Windows.bgColorYellowLight).grid(row=0, column=1, padx=10, sticky="w")

        tk.Button(self, text="Importer des\nmusiques", command=self.importerMusic, font=Windows.police, fg=Windows.fgColorBlack, bg=Windows.bgColorGreen, activebackground=Windows.bgColorGreenLight).grid(row=0, column=2, padx=10, sticky="w")

        self.lblDuree = tk.Label(self, text=f"Musiques : {len(self.playlist.listeMusic)} | Durée : {self.playlist.calcDuree().__str__()}", font=Windows.police, bg=Windows.bgColorGreyLight, fg=Windows.fgColorWhite)
        self.lblDuree.grid(row=0, column=2, sticky="e", padx=10)

        self.rightClickMenu = tk.Menu(tearoff=0)
        self.rightClickMenu.add_command(label="Ajouter à la file d'attente", command=self.ajouterQueue)
        self.rightClickMenu.add_command(label="Lire ensuite", command=self.lireEnsuite)
        self.rightClickMenu.add_command(label="Ajouter à une playlist", command=self.ajouterAUnePlaylist)
        self.rightClickMenu.add_command(label="Supprimer définitivement", command=self.supprimerMusic)

        scrollbar = tk.Scrollbar(self)
        scrollbar.grid(row=1, column=3, sticky="nse")

        self.lbPlaylist = treectrl.MultiListbox(self, command=self.ajouterQueue, columns=("Titre", "Artistse", "Album", "Durée"), selectmode='extended', expandcolumns=(1, 2, 3, 4), bg=Windows.bgColorGreyLight, fg=Windows.fgColorWhite, font=Windows.police, selectbackground=Windows.bgColorYellow, selectforeground="black")
        self.lbPlaylist.grid(row=1, column=0, sticky="nsew", columnspan=3)
        self.lbPlaylist.configure(headerfg="black", headerfont=Windows.police, scrollmargin=10, itemheight=30, yscrollcommand=scrollbar.set)
        scrollbar.config(command=self.lbPlaylist.yview)

        colors = (Windows.bgColorGrey, Windows.bgColorGreyLight)
        self.lbPlaylist.column_configure(self.lbPlaylist.column(0), itembackground=colors, width=400, arrow='down', arrowgravity='right')
        self.lbPlaylist.column_configure(self.lbPlaylist.column(1), itembackground=colors, arrow='down', arrowgravity='right')
        self.lbPlaylist.column_configure(self.lbPlaylist.column(2), itembackground=colors, arrow='down', arrowgravity='right')
        self.lbPlaylist.column_configure(self.lbPlaylist.column(3), itembackground=colors, arrow='down', arrowgravity='right')

        self.lbPlaylist.bind("<3>", lambda e: self.context_menu(e))

        self.lbPlaylist.notify_install('<Header-invoke>')
        self.lbPlaylist.notify_bind('<Header-invoke>', self.sort_list)

        self.sortorder_flags = {0: 'increasing', 1: 'increasing', 2: 'increasing', 3: 'increasing'}

        self.afficherPlaylist()

    def sort_by_duree(self, item1, item2):
        i1, i2 = self.lbPlaylist.index(item=item1), self.lbPlaylist.index(item=item2)
        a = self.playlist.getMusicByName(self.lbPlaylist.get(i1)[0][0])
        b = self.playlist.getMusicByName(self.lbPlaylist.get(i2)[0][0])

        if a.duree.totSec > b.duree.totSec:
            return 1
        elif a.duree.totSec < b.duree.totSec:
            return -1
        else:
            return 0

    def sort_list(self, event):
        # do the sorting
        if event.column == 0:
            self.lbPlaylist.sort(column=0, mode=self.sortorder_flags[0])
        elif event.column == 1:
            self.lbPlaylist.sort(column=1, mode=self.sortorder_flags[1])
        elif event.column == 2:
            self.lbPlaylist.sort(column=2, mode=self.sortorder_flags[2])
        else:
            self.lbPlaylist.sort(column=3, command=self.sort_by_duree, mode=self.sortorder_flags[3])
        # switch the sortorder flag and turn the arrow icons upside down
        if self.sortorder_flags[event.column] == 'increasing':
            self.lbPlaylist.column_configure(self.lbPlaylist.column(event.column), arrow='up')
            self.sortorder_flags[event.column] = 'decreasing'
        else:
            self.lbPlaylist.column_configure(self.lbPlaylist.column(event.column), arrow='down')
            self.sortorder_flags[event.column] = 'increasing'

    def afficherPlaylist(self):
        self.lblDuree["text"] = f"Musiques : {len(self.playlist.listeMusic)} | Durée : {self.playlist.calcDuree().__str__()}"
        self.lbPlaylist.delete(0, "end")
        for index, music in enumerate(self.playlist.listeMusic):
            self.lbPlaylist.insert(index, music, music.artiste or " ", music.album or " ", music.duree)

    def lireAlea(self):
        listeAlea = random.sample(self.playlist.listeMusic, len(self.playlist.listeMusic))
        for music in listeAlea:
            self.queue.ajouterQueue(music)

    def lire(self):
        for music in self.playlist.listeMusic:
            self.queue.ajouterQueue(music)

    def ajouterAUnePlaylist(self, ind=None):
        dialog = ComboDialog(self, self.gest)
        nom = dialog.result
        if nom:
            for i in self.lbPlaylist.curselection():
                self.gest.getPlayListByName(nom).ajouterMusic(self.playlist.getMusicByName(self.lbPlaylist.get(i)[0][0]))

    def ajouterQueue(self, ind=None):
        for i in self.lbPlaylist.curselection():
            self.queue.ajouterQueue(self.playlist.getMusicByName(self.lbPlaylist.get(i)[0][0]))

    def lireEnsuite(self):
        for i in self.lbPlaylist.curselection():
            self.queue.lireEnsuite(self.playlist.getMusicByName(self.lbPlaylist.get(i)[0][0]))

    def context_menu(self, event):
        if self.lbPlaylist.curselection():
            self.rightClickMenu.post(event.x_root, event.y_root)

    def importerMusic(self):
        folder = filedialog.askdirectory()
        if folder:
            # copie dans le dossier music
            musicsAImporter = os.listdir(folder)
            print(musicsAImporter)
            for fichier in musicsAImporter:
                if fichier[-4:] == ".mp3":
                    shutil.copyfile(f"{folder}/{fichier}", f"./music/{fichier}")

            # ajout à la playlist all
            self.gest.getPlayListByName("all").ajouterMusicDl(suprSonAbsent=False)
        self.afficherPlaylist()

    def supprimerMusic(self, ind=None):
        rep = tk.messagebox.askokcancel("Suppression de musique.s", f"La.es musique.s séléctionnnée.s va.ont être supprimée.s.\nAttention cette opération est définitive !")
        if rep:
            for i in self.lbPlaylist.curselection():
                if self.gest.getPlayListByName("all").getMusicByName(self.lbPlaylist.get(i)[0][0]) in self.queue.listeMusic:
                    self.queue.enleverQueue(self.gest.getPlayListByName("all").getMusicByName(self.lbPlaylist.get(i)[0][0]))
                self.gest.supprimerMusic(self.gest.getPlayListByName("all").getMusicByName(self.lbPlaylist.get(i)[0][0]))
            self.afficherPlaylist()


class FenQueue(tk.Frame):
    def __init__(self, parent, queue):
        tk.Frame.__init__(self, parent, bg=Windows.bgColorGreyLight)
        self.queue = queue

        self.grid_columnconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=1)
        self.grid_columnconfigure(2, weight=1)

        self.grid_rowconfigure(0, weight=1)
        self.grid_rowconfigure(1, weight=3)

        tk.Label(self, text="Liste de lecture", font=Windows.policeTitre, bg=Windows.bgColorGreyLight, fg=Windows.fgColorWhite).grid(row=0, column=0, sticky="nsw", padx=20)
        tk.Button(self, text="Effacer la liste de lecture", font=Windows.police, fg=Windows.fgColorBlack, bg=Windows.bgColorRed, activebackground=Windows.bgColorRedLight, command=lambda: self.viderLaQueue()).grid(row=0, column=0, sticky="e", padx=10)

        self.lblDuree = tk.Label(self, text=f"Musiques : {len(self.queue.listeMusic)} | Durée : {self.queue.calcDuree().__str__()}", font=Windows.police, bg=Windows.bgColorGreyLight, fg=Windows.fgColorWhite)
        self.lblDuree.grid(row=0, column=2, sticky="e", padx=10)

        self.rightClickMenu = tk.Menu(tearoff=0)
        self.rightClickMenu.add_command(label="Lire maintenant", command=self.setMusicMtn)
        self.rightClickMenu.add_command(label="Suprimer de la file d'attente", command=self.enleverQueue)

        self.scrollbar = tk.Scrollbar(self)
        self.scrollbar.grid(row=1, column=3, sticky="nse")

        self.lbQueue = treectrl.MultiListbox(self, command=self.setMusicMtn, columns=("Titre", "Artistse", "Album", "Durée"), selectmode='extended', expandcolumns=(1, 2, 3, 4), bg=Windows.bgColorGreyLight, fg=Windows.fgColorWhite, font=Windows.police, selectbackground=Windows.bgColorYellow, selectforeground="black")
        self.lbQueue.grid(row=1, column=0, sticky="nsew", columnspan=3)
        self.lbQueue.configure(headerfg="black", headerfont=Windows.police, scrollmargin=10, itemheight=30, yscrollcommand=self.scrollbar.set)
        self.scrollbar.config(command=self.lbQueue.yview)

        colors = (Windows.bgColorGrey, Windows.bgColorGreyLight)
        self.lbQueue.column_configure(self.lbQueue.column(0), itembackground=colors, width=400, arrow='down', arrowgravity='right')
        self.lbQueue.column_configure(self.lbQueue.column(1), itembackground=colors, arrow='down', arrowgravity='right')
        self.lbQueue.column_configure(self.lbQueue.column(2), itembackground=colors, arrow='down', arrowgravity='right')
        self.lbQueue.column_configure(self.lbQueue.column(3), itembackground=colors, arrow='down', arrowgravity='right')

        self.lbQueue.bind("<3>", lambda e: self.context_menu(e))

        self.lbQueue.notify_install('<Header-invoke>')
        self.lbQueue.notify_bind('<Header-invoke>', self.sort_list)

        self.sortorder_flags = {0: 'increasing', 1: 'increasing', 2: 'increasing', 3: 'increasing'}

        self.afficherQueue()

    def sort_by_duree(self, item1, item2):
        i1, i2 = self.lbQueue.index(item=item1), self.lbQueue.index(item=item2)
        a = self.queue.getMusicByName(self.lbQueue.get(i1)[0][0])
        b = self.queue.getMusicByName(self.lbQueue.get(i2)[0][0])

        if a.duree.totSec > b.duree.totSec:
            return 1
        elif a.duree.totSec < b.duree.totSec:
            return -1
        else:
            return 0

    def sort_list(self, event):
        # do the sorting
        if event.column == 0:
            self.lbQueue.sort(column=0, mode=self.sortorder_flags[0])
        elif event.column == 1:
            self.lbQueue.sort(column=1, mode=self.sortorder_flags[1])
        elif event.column == 2:
            self.lbQueue.sort(column=2, mode=self.sortorder_flags[2])
        else:
            self.lbQueue.sort(column=3, command=self.sort_by_duree, mode=self.sortorder_flags[3])
        # switch the sortorder flag and turn the arrow icons upside down
        if self.sortorder_flags[event.column] == 'increasing':
            self.lbQueue.column_configure(self.lbQueue.column(event.column), arrow='up')
            self.sortorder_flags[event.column] = 'decreasing'
        else:
            self.lbQueue.column_configure(self.lbQueue.column(event.column), arrow='down')
            self.sortorder_flags[event.column] = 'increasing'

    def afficherQueue(self):
        self.lblDuree["text"] = f"Musiques : {len(self.queue.listeMusic)} | Durée : {self.queue.calcDuree().__str__()}"
        self.lbQueue.delete(0, "end")
        for index, music in enumerate(self.queue.listeMusic):
            self.lbQueue.insert(index, music, music.artiste or " ", music.album or " ", music.duree)

    def setMusicMtn(self, ind=None):
        if ind is None:
            self.queue.setMusicMtn(self.lbQueue.curselection()[0])
        else:
            self.queue.setMusicMtn(ind)

    def enleverQueue(self, ind=None):
        for i in self.lbQueue.curselection():
            self.queue.enleverQueue(self.queue.getMusicByName(self.lbQueue.get(i)[0][0]))
        self.afficherQueue()

    def viderLaQueue(self):
        self.queue.viderLaQueue()
        self.afficherQueue()

    def context_menu(self, event):
        if self.lbQueue.curselection():
            self.rightClickMenu.post(event.x_root, event.y_root)

    def afficherEtatThread(self):
        print(self.queue.thread.isAlive())


class FenPlaylist(tk.Frame):
    def __init__(self, parent, playlist, queue, gest):
        tk.Frame.__init__(self, parent, bg=Windows.bgColorGreyLight)
        self.playlist = playlist
        self.queue = queue
        self.gest = gest

        self.grid_columnconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=1)
        self.grid_columnconfigure(2, weight=1)

        self.grid_rowconfigure(0, weight=1)
        self.grid_rowconfigure(1, weight=3)

        tk.Label(self, text=f"Playlist : {self.playlist.nom}", font=Windows.policeTitre, bg=Windows.bgColorGreyLight, fg=Windows.fgColorWhite).grid(row=0, column=0, sticky="nsw", padx=20)
        tk.Button(self, text="Tout ajouter\nà la liste de lecture", command=self.lire, font=Windows.police, fg=Windows.fgColorBlack, bg=Windows.bgColorYellow, activebackground=Windows.bgColorYellowLight).grid(row=0, column=0, padx=10, sticky="e")
        tk.Button(self, text="Tout ajouter\nen aléatoire à la liste de lecture", command=self.lireAlea, font=Windows.police, fg=Windows.fgColorBlack, bg=Windows.bgColorYellow, activebackground=Windows.bgColorYellowLight).grid(row=0, column=1, padx=10, sticky="w")

        self.lblDuree = tk.Label(self, text=f"Musiques : {len(self.playlist.listeMusic)} | Durée : {self.playlist.calcDuree().__str__()}", font=Windows.police, bg=Windows.bgColorGreyLight, fg=Windows.fgColorWhite)
        self.lblDuree.grid(row=0, column=2, sticky="e", padx=10)

        tk.Button(self, text="Exporter les musiques", command=self.exporterMusiques, font=Windows.police, fg=Windows.fgColorBlack, bg=Windows.bgColorYellow, activebackground=Windows.bgColorYellowLight).grid(row=0, column=2, sticky="w", padx=10)

        self.rightClickMenu = tk.Menu(tearoff=0)
        self.rightClickMenu.add_command(label="Ajouter à la file d'attente", command=self.ajouterQueue)
        self.rightClickMenu.add_command(label="Lire ensuite", command=self.lireEnsuite)
        self.rightClickMenu.add_command(label="Ajouter à une playlist", command=self.ajouterAUnePlaylist)
        self.rightClickMenu.add_command(label="Supprimer de la playlist", command=self.supprimerDeLaPlaylist)

        scrollbar = tk.Scrollbar(self)
        scrollbar.grid(row=1, column=3, sticky="nse")

        self.lbPlaylist = treectrl.MultiListbox(self, command=self.ajouterQueue, columns=("Titre", "Artistse", "Album", "Durée"), selectmode='extended', expandcolumns=(1, 2, 3, 4), bg=Windows.bgColorGreyLight, fg=Windows.fgColorWhite, font=Windows.police, selectbackground=Windows.bgColorYellow, selectforeground="black")
        self.lbPlaylist.grid(row=1, column=0, sticky="nsew", columnspan=3)
        self.lbPlaylist.configure(headerfg="black", headerfont=Windows.police, scrollmargin=10, itemheight=30, yscrollcommand=scrollbar.set)
        scrollbar.config(command=self.lbPlaylist.yview)

        colors = (Windows.bgColorGrey, Windows.bgColorGreyLight)
        self.lbPlaylist.column_configure(self.lbPlaylist.column(0), itembackground=colors, width=400, arrow='down', arrowgravity='right')
        self.lbPlaylist.column_configure(self.lbPlaylist.column(1), itembackground=colors, arrow='down', arrowgravity='right')
        self.lbPlaylist.column_configure(self.lbPlaylist.column(2), itembackground=colors, arrow='down', arrowgravity='right')
        self.lbPlaylist.column_configure(self.lbPlaylist.column(3), itembackground=colors, arrow='down', arrowgravity='right')

        self.lbPlaylist.bind("<3>", lambda e: self.context_menu(e))

        self.lbPlaylist.notify_install('<Header-invoke>')
        self.lbPlaylist.notify_bind('<Header-invoke>', self.sort_list)

        self.sortorder_flags = {0: 'increasing', 1: 'increasing', 2: 'increasing', 3: 'increasing'}

        self.afficherPlaylist()

    def sort_by_duree(self, item1, item2):
        i1, i2 = self.lbPlaylist.index(item=item1), self.lbPlaylist.index(item=item2)
        a = self.playlist.getMusicByName(self.lbPlaylist.get(i1)[0][0])
        b = self.playlist.getMusicByName(self.lbPlaylist.get(i2)[0][0])

        if a.duree.totSec > b.duree.totSec:
            return 1
        elif a.duree.totSec < b.duree.totSec:
            return -1
        else:
            return 0

    def sort_list(self, event):
        # do the sorting
        if event.column == 0:
            self.lbPlaylist.sort(column=0, mode=self.sortorder_flags[0])
        elif event.column == 1:
            self.lbPlaylist.sort(column=1, mode=self.sortorder_flags[1])
        elif event.column == 2:
            self.lbPlaylist.sort(column=2, mode=self.sortorder_flags[2])
        else:
            self.lbPlaylist.sort(column=3, command=self.sort_by_duree, mode=self.sortorder_flags[3])
        # switch the sortorder flag and turn the arrow icons upside down
        if self.sortorder_flags[event.column] == 'increasing':
            self.lbPlaylist.column_configure(self.lbPlaylist.column(event.column), arrow='up')
            self.sortorder_flags[event.column] = 'decreasing'
        else:
            self.lbPlaylist.column_configure(self.lbPlaylist.column(event.column), arrow='down')
            self.sortorder_flags[event.column] = 'increasing'

    def afficherPlaylist(self):
        self.lblDuree["text"] = f"Musiques : {len(self.playlist.listeMusic)} | Durée : {self.playlist.calcDuree().__str__()}"
        self.lbPlaylist.delete(0, "end")
        for index, music in enumerate(self.playlist.listeMusic):
            self.lbPlaylist.insert(index, music, music.artiste or " ", music.album or " ", music.duree)

    def lireAlea(self):
        listeAlea = random.sample(self.playlist.listeMusic, len(self.playlist.listeMusic))
        for music in listeAlea:
            self.queue.ajouterQueue(music)

    def lire(self):
        for music in self.playlist.listeMusic:
            self.queue.ajouterQueue(music)

    def supprimerDeLaPlaylist(self, ind=None):
        for i in self.lbPlaylist.curselection():
            self.playlist.enleverMusic(self.playlist.getMusicByName(self.lbPlaylist.get(i)[0][0]))
        self.afficherPlaylist()

    def ajouterAUnePlaylist(self, ind=None):
        dialog = ComboDialog(self, self.gest)
        nom = dialog.result
        if nom:
            for i in self.lbPlaylist.curselection():
                self.gest.getPlayListByName(nom).ajouterMusic(self.playlist.getMusicByName(self.lbPlaylist.get(i)[0][0]))

    def ajouterQueue(self, ind=None):
        for i in self.lbPlaylist.curselection():
            self.queue.ajouterQueue(self.playlist.getMusicByName(self.lbPlaylist.get(i)[0][0]))

    def lireEnsuite(self):
        for i in self.lbPlaylist.curselection():
            self.queue.lireEnsuite(self.playlist.getMusicByName(self.lbPlaylist.get(i)[0][0]))

    def context_menu(self, event):
        if self.lbPlaylist.curselection():
            self.rightClickMenu.post(event.x_root, event.y_root)

    def exporterMusiques(self):
        folder = filedialog.askdirectory()
        if folder:
            self.playlist.copyToFolder(folder)


class FenRechercher(tk.Frame):
    def __init__(self, parent, gest):
        tk.Frame.__init__(self, parent)
        self.grid_columnconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=1)
        self.grid_columnconfigure(2, weight=4)

        self.grid_rowconfigure(0, weight=3)
        self.grid_rowconfigure(1, weight=1)
        self.grid_rowconfigure(2, weight=1)
        self.grid_rowconfigure(3, weight=5)

        tk.Label(self, text="Rechercher", font="italic 15 bold").grid(row=0, column=0, columnspan=3)


class ComboDialog(Dialog):

    def __init__(self, parent, gest, okTxt="Ok", cancelTxt="Cancel"):
        self.gest = gest
        self.result = None
        self.cbPlaylist = None
        self.okTxt = okTxt
        self.cancelTxt = cancelTxt
        self.selectedItem = tk.StringVar()
        super().__init__(parent, title="Ajouter à une playlist")

    def body(self, master):
        self.cbPlaylist = Combobox(master, textvariable=self.selectedItem, state="readonly")
        self.cbPlaylist["values"] = self.gest.getPlaylistsName()
        if len(self.gest.getPlaylistsName()) > 0:
            self.cbPlaylist.current(newindex=0)
        self.cbPlaylist.pack()
        return self.cbPlaylist

    def buttonbox(self):
        box = Frame(self)

        w = Button(box, text=self.okTxt, width=10, command=self.ok, default=tk.ACTIVE)
        w.pack(side=tk.LEFT, padx=5, pady=5)
        w = Button(box, text=self.cancelTxt, width=10, command=self.cancel)
        w.pack(side=tk.LEFT, padx=5, pady=5)

        self.bind("<Return>", self.ok)
        self.bind("<Escape>", self.cancel)

        box.pack()

    def apply(self):
        self.result = self.cbPlaylist.get()


class CustomAksString(Dialog):

    def __init__(self, parent, title, text):
        self.result = None
        self.entry_widget = None
        self.text = text
        super().__init__(parent, title=title)

    def body(self, master):
        tk.Label(master, text=self.text).pack()
        vcmd = (self.register(self.onValidate), '%d', '%i', '%P', '%s', '%S', '%v', '%V', '%W')
        self.entry_widget = tk.Entry(master, validate="key", validatecommand=vcmd)
        self.entry_widget.pack()
        return self.entry_widget

    def onValidate(self, d, i, P, s, S, v, V, W):
        if int(i) < 10:
            return True
        else:
            self.bell()
            return False

    def apply(self):
        self.result = self.entry_widget.get()


class TimeBarScale(Scale):

    def __init__(self, master=None, **kw):
        Scale.__init__(self, master, **kw)
        self._style_name = '{}.custom.{}.TScale'.format(self, kw['orient'].capitalize())  # unique style name to handle the text
        self['style'] = self._style_name


def addTreeCtrlToTk():
    try:
        shutil.copytree("./treectrl2.4.1", "./lib/tkinter/treectrl2.4.1")
    except Exception as e:
        print(f"Erreur : {e}")
    shutil.rmtree("./treectrl2.4.1")


if not os.path.exists('music'):
    os.mkdir('music')
if not os.path.exists('playlist'):
    os.mkdir('playlist')
if os.path.exists('treectrl2.4.1'):
    try:
        addTreeCtrlToTk()
    except Exception as e:
        print(f"Erreur : {e}")

if not os.path.exists('playlist/all'):
    allMusic = PlaylistDl("all")

gestion = GestionPlaylist()
gestion.discoverPlaylist()

pygame.mixer.init(44100, -16, 2, 512)

fen = Windows(gestion)
fen.mainloop()

print("affichage :\n" + gestion.__str__())
