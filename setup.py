from cx_Freeze import Executable, setup

executables = [
    Executable("main.py",
               shortcut_name="MeatMusic",
               shortcut_dir="DesktopFolder",
               icon="./playButton.ico"
               )
]  # , base="Win32GUI"

bdist_msi_options = {
    "install_icon": "./playButton.ico",
    "upgrade_code": "{8FE984E9-3670-44C2-8295-6E8D1022C657}",
}

build_exe_options = {
    'includes': ['moviepy.editor', 'mutagen.easyid3', 'mutagen.mp3', 'pytube', 'ytmusicapi', 'pygame', 'tkinter',
                 'tkinter.messagebox', 'tkinter.simpledialog', 'tkinter.ttk', 'tkinter', 'TkTreectrl'],
    'include_files': ['playButton.ico',
                      r'./tcl/treectrl2.4.1/',
                      r'./images/',
                      'config.txt'
                      ],
}

setup(
    name="MeatMusic",
    version="0.1",
    description="MeatMusic",
    executables=executables,
    options={
        "bdist_msi": bdist_msi_options,
        'build_exe': build_exe_options,
    },
)
