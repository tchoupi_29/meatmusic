# MeatMusic
C'est une super application qui permet de télécharger des playlists de musique de youtube.
En plus de ça il y a un player qui peut gérer des playlists locales et jouer les musiques téléchargées.

## Installation

### - Vous êtes sous Windows et vous avez confiance en moi
C'est normal je suis le meilleur. Pour installer ce magnifique logiciel il suffit de télécharger le fichier d'installation `MeatMusic-X.X-win32.msi` qui se trouve dans le dossier `dist/`.
Ensuiste il suffit de le lancer et de suivre les instructions de l'installeur.

### - Vous préférez mettre en pratique vos compétences d'ingénieur informaticien
Pour commencer il faut cloner le dépot avec `git clone`. Puis vous devez ajouter un fichier `pyvenv.cfg` dans le dossier `venv` avec ces lignes (à modifier en fonction de votre environement) :
  ```
  home = <location-of-python-executable>
  include-system-site-packages = false
  version = <python-version>
  ```
Ensuite vous avez deux choix :

- #### Complilation (Windows)
  Il faut se placer à la racine du dépot et lancer la commande : `venv\Scripts\python.exe setup.py bdist_msi`. Et voilà le fichier d'installation est créé dans le dossier `dist/` et il suffit de le lancer et de suivre ses instructions.

- #### Interprétation Python3 (Windows/Linux)
  Malheurseument une dépendance de plus qu'il faut placer manuellement dans le dossier `tcl/` à la racine de votre installation de python3. Tous ses fichiers se trouvent dans le dossier `tcl/treectrl2.4.1/` de ce dépôt git. Ensuite, il faut se placer à la racine du dépot et lancer la commande : `venv\Scripts\python.exe main.py`. Et voilà normalement MeatMusic se lance sans problème.

#### Dépendances
- python3
  - tkinter
  - os
  - random
  - time
  - shutil
  - pickle
  - threading